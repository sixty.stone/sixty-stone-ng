import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISixtyStoneTreeItemBase, SixtyStoneTreeItem, SixtyStoneTreeViewParams, SixtyStoneTreeViewIconPaths } from 'dist/sixty-stone-tree-view';

@Component({
  selector: 'tree-demo',
  templateUrl: './tree-demo.component.html',
  styleUrls: ['./tree-demo.component.sass']
})
export class TreeDemoComponent implements OnInit {

  treeViewParams: SixtyStoneTreeViewParams = SixtyStoneTreeViewParams.emptyParams();

  ngOnInit() : void {
    this.http.get<Array<ISixtyStoneTreeItemBase>>("assets/tree-demo.json").subscribe(response => {
      const id = "42";
      const tempItems = response.map(r => new SixtyStoneTreeItem(r, true));
      tempItems.forEach(item => item.TreeId = id);
      const tempParams = new SixtyStoneTreeViewParams(id, tempItems);
      tempParams.EventHandlers.OnNodeToggle = (node: SixtyStoneTreeItem) => console.log(node);
      tempParams.IconPaths.FolderIconPath = "../assets/images/folder.gif";
      tempParams.IconPaths.OpenFolderIconPath = "../assets/images/open.gif";
      tempParams.IconPaths.PlusIconPath = "../assets/images/plus.gif";
      tempParams.IconPaths.MinusIconPath = "../assets/images/minus.gif";
      tempParams.IconPaths.NodeIconPath = "../assets/images/file.gif";
      tempParams.IconPaths.LineIconPath = "../assets/images/line.gif";
      this.treeViewParams = tempParams;
    });
  }

  constructor(private http: HttpClient) { }
}
