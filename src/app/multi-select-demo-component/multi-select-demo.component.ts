import { Component, OnInit } from '@angular/core';
import { SixtyStoneMultiSelectParams } from '@sixty.stone/ngx-sixty-stone-multi-select';
import { SixtyStoneListItem } from '@sixty.stone/ngx-sixty-stone-common';

@Component({
  selector: 'multi-select-demo',
  templateUrl: './multi-select-demo.component.html',
  styleUrls: ['./multi-select-demo.component.sass']
})
export class MultiSelectDemoComponent implements OnInit {

  multiSelectParams: SixtyStoneMultiSelectParams<DemoListItem> = SixtyStoneMultiSelectParams.emptyParams();

  ngOnInit(): void {
    this.multiSelectParams = new SixtyStoneMultiSelectParams("73", [
      {value: 1, text: "Demo Item 1", isSelected: false, parent: new DemoListItem()},
      {value: 2, text: "Demo Item 2", isSelected: false, parent: new DemoListItem()},
      {value: 4, text: "Demo Item 4", isSelected: false, parent: new DemoListItem()},
      {value: 5, text: "Demo Item 5", isSelected: false, parent: new DemoListItem()}
    ], [{value: 3, text: "Demo Item 3", isSelected: false, parent: new DemoListItem()}]);

    this.multiSelectParams.eventHandlers.onChange = (addedItems: Array<SixtyStoneListItem<DemoListItem>>,
      removedItems: Array<SixtyStoneListItem<DemoListItem>>) => console.log({addedItems, removedItems});
  }

  constructor() { }
}

class DemoListItem {}
