import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SixtyStoneTableParams, SixtyStoneTableColumn, SixtyStoneTableFilter, SixtyStoneTableSavedFilter } from '@sixty.stone/ngx-sixty-stone-table';
import { SixtyStoneCommonHelper } from '@sixty.stone/ngx-sixty-stone-common';

@Component({
  selector: 'table-demo-component',
  templateUrl: './table-demo.component.html',
  styleUrls: ['./table-demo.component.sass']
})
export class TableDemoComponent implements OnInit {

  tableParams: SixtyStoneTableParams = SixtyStoneTableParams.emptyParams();

  ngOnInit() : void {
    this.http.get<Array<any>>("assets/table-demo.json").subscribe(response => {
      this.tableParams = new SixtyStoneTableParams("42", response);
      this.tableParams.eventHandlers.onCellClicked = (column: SixtyStoneTableColumn, row: any) => {
        alert(JSON.stringify({column, row}));
        console.log(this.commonHelper.clone(column));
        console.log(this.commonHelper.clone(row));
      };
      this.tableParams.savedFilters = [
        new SixtyStoneTableSavedFilter(13, "Saved Filter 1", [
          new SixtyStoneTableFilter("Column_4", "85"),
          new SixtyStoneTableFilter("Column_1", "13")
        ]),
        new SixtyStoneTableSavedFilter(14, "Saved Filter 2", [
          new SixtyStoneTableFilter("Column_2", "67"),
          new SixtyStoneTableFilter("Column_5", "96")
        ])
      ];
    });
  }

  constructor(private http: HttpClient, private commonHelper: SixtyStoneCommonHelper) { }
}
