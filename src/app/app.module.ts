import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app-component/app.component';
import { SixtyStoneCommonModule } from '@sixty.stone/ngx-sixty-stone-common';
import { SixtyStoneTableModule } from '@sixty.stone/ngx-sixty-stone-table';
import { SixtyStoneMultiSelectModule } from '@sixty.stone/ngx-sixty-stone-multi-select';
import { TableDemoComponent } from './table-demo-component/table-demo.component';
import { MultiSelectDemoComponent } from './multi-select-demo-component/multi-select-demo.component';
import { TreeDemoComponent } from './tree-demo-component/tree-demo.component';
import { SixtyStoneTreeViewModule } from 'dist/sixty-stone-tree-view';

@NgModule({
  declarations: [
    AppComponent,
    TableDemoComponent,
    MultiSelectDemoComponent,
    TreeDemoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SixtyStoneTableModule,
    SixtyStoneCommonModule,
    SixtyStoneMultiSelectModule,
    AppRoutingModule,
    SixtyStoneTreeViewModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
