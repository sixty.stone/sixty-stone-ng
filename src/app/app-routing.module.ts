import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableDemoComponent } from './table-demo-component/table-demo.component';
import { MultiSelectDemoComponent } from './multi-select-demo-component/multi-select-demo.component';
import { TreeDemoComponent } from './tree-demo-component/tree-demo.component';

const routes: Routes = [
  {path: 'table', component: TableDemoComponent},
  {path: 'multi-select', component: MultiSelectDemoComponent},
  {path: 'tree-view', component: TreeDemoComponent},
  {path: '', redirectTo: '/table', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
