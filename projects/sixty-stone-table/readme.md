# @sixty.stone/ngx-sixty-stone-table

## Dependencies

* `npm install @sixty.stone/ngx-sixty-stone-pager --save`

* `npm install @sixty.stone/ngx-sixty-stone-dialog --save`

## Implementation

### Bootstrap Module

* `import { SixtyStoneTableModule } from '@sixty.stone/ngx-sixty-stone-table'`

### App Component TypeScript

* `import { SixtyStoneTableParams } from '@sixty.stone/ngx-sixty-stone-table'`

* `tableParams: SixtyStoneTableParams = SixtyStoneTableParams.emptyParams();`

### Inside ngOnInit()

* `this.tableParams = new SixtyStoneTableParams(id, data);`

* `this.tableParams.eventHandlers.onCellClicked = (column, row) => console.log({column, row});`

### App Component HTML

* `<sixty-stone-table [table-params]="tableParams"></sixty-stone-table>`
