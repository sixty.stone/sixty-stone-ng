/*
 * Public API Surface of sixty-stone-table
 */

export * from './lib/sixty-stone-table-component/sixty-stone-table.component';
export * from './lib/sixty-stone-table.classes';
export * from './lib/sixty-stone-table.module';
