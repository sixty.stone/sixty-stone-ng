import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { SixtyStoneDialogComponent, SixtyStoneDialogParams, SixtyStoneDialogSize } from '@sixty.stone/ngx-sixty-stone-dialog';
import { SixtyStoneTableHelper } from '../sixty-stone-table-helper/sixty-stone-table-helper.service';
import { SixtyStonePager, SixtyStonePagerParams } from '@sixty.stone/ngx-sixty-stone-pager';
import { SixtyStoneTable, SixtyStoneTableColumn, SixtyStoneTableFilter, SixtyStoneTableStateData, SixtyStoneTableParams,
  SixtyStoneTableSavedFilter } from '../sixty-stone-table.classes';

@Component({
  selector: 'sixty-stone-table',
  templateUrl: './sixty-stone-table.component.html',
  styleUrls: ['./sixty-stone-table.component.sass']
})
export class SixtyStoneTableComponent implements OnInit {

  @Input("table-params") tableParams: SixtyStoneTableParams = SixtyStoneTableParams.emptyParams();

  private filtersDialog : SixtyStoneDialogComponent | null = null;

  pagerParams: SixtyStonePagerParams = SixtyStonePagerParams.emptyParams();
  dialogParams: SixtyStoneDialogParams = new SixtyStoneDialogParams(SixtyStoneDialogSize.medium, "Filters");
  table: SixtyStoneTable = new SixtyStoneTable("", []);

  updateGrid(filterColumnId: string, filterText: string, sortColumn?: SixtyStoneTableColumn) : void {
    if (!sortColumn) {
      this.table.selectedSavedFilterId = 0;
    }
    this.updateTable(filterColumnId, filterText, sortColumn);
  }

  cellClicked(column: SixtyStoneTableColumn, row: any) : void {
    this.tableParams.eventHandlers.onCellClicked(column, row);
  }

  onFilterColumnChanged(filterSelect: any) : void {
    let filterText = "";
    if (this.table.filters.some(f => f.columnId == filterSelect.value)) {
      filterText = this.table.filters.filter(f => f.columnId == filterSelect.value)[0].filterText; 
    }
    this.table.filterText = filterText;
    this.table.selectedFilterColumnId = filterSelect.value;
    this.updateTableStateData();
  }

  onSavedFilterChanged(savedFilterSelect: any) : void {
    const selectedValue = parseInt(savedFilterSelect.value);
    if (selectedValue === 0) {
      this.clearFilters();
      return;
    }
    const filter = this.table.savedFilters.filter(f => f.id === selectedValue)[0];
    if (filter.filters.length === 0) {
      return;
    }
    const first = filter.filters[0];
    this.table.selectedSavedFilterId = selectedValue;
    this.table.filters = [];
    this.table.filterText = "";
    filter.filters.forEach(f => this.table.filters.push(new SixtyStoneTableFilter(f.columnId, f.filterText)));
    this.updateTable(first.columnId, first.filterText);
  }

  showFilters($event: Event) : void {
    $event.preventDefault();
    this.filtersDialog?.open();
  }

  clearFilter(filter: SixtyStoneTableFilter) : void {
    this.table.selectedSavedFilterId = 0;
    if (this.table.selectedFilterColumnId === filter.columnId) {
      this.table.filterText = "";
    }
    this.updateTable(filter.columnId, "");
  }

  clearFilters() : void {
    this.table.selectedSavedFilterId = 0;
    this.table.filters = [];
    this.table.filterText = "";
    this.updateTable(this.table.columns[0].columnId, "");
  }

  ngOnInit(): void {
    this.dialogParams.eventHandlers.onInit = (dialogBox: SixtyStoneDialogComponent) => {
      this.filtersDialog = dialogBox;
    }
    this.buildTable(this.tableParams);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tableParams && typeof changes.tableParams.currentValue === "object") {
      this.buildTable(changes.tableParams.currentValue);
    }
  }

  private updateTable(filterColumnId: string, filterText: string, sortColumn?: SixtyStoneTableColumn) : void {
    this.tableHelper.updateTable(this.table, filterColumnId, filterText, sortColumn);
    this.buildPager();
  }

  private buildTable(tableParams: SixtyStoneTableParams) : void {
    if (tableParams.savedFilters.length > 0) {
      tableParams.savedFilters.splice(0, 0, new SixtyStoneTableSavedFilter(0, "-- Select --", []));
    }
    if (this.tableHelper.stateData.some(g => g.id === tableParams.id)) {
      tableParams.stateData = this.tableHelper.stateData.filter(g => g.id === tableParams.id)[0];
    }
    this.table = this.tableHelper.buildTable(tableParams);
    this.buildPager();
  }

  private buildPager() : void {
    const newPagerParams: SixtyStonePagerParams = new SixtyStonePagerParams(this.table.filteredRows.length, this.tableParams.rowsPerPage, this.tableParams.pageNumberDisplayCount,
      this.tableParams.stateData?.currentPage || 1, this.tableParams.stateData?.firstDisplayedPageNumber || 1);
    newPagerParams.eventHandlers.onPageEvent = (pager: SixtyStonePager) : void => {
      this.table.pager = pager;
      this.tableHelper.refreshDisplayedRows(this.table);
      this.updateTableStateData();
    };
    this.pagerParams = newPagerParams;
  }

  private updateTableStateData() : void {
    this.tableHelper.stateData = this.tableHelper.stateData.filter(g => g.id !== this.table.id);
    this.tableHelper.stateData.push(new SixtyStoneTableStateData(this.table));
  }

  constructor(private tableHelper: SixtyStoneTableHelper) { }
}
