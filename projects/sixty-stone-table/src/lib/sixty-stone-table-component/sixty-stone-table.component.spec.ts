import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SixtyStoneTableComponent } from './sixty-stone-table.component';

describe('SixtyStoneTableComponent', () => {
  let component: SixtyStoneTableComponent;
  let fixture: ComponentFixture<SixtyStoneTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SixtyStoneTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SixtyStoneTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
