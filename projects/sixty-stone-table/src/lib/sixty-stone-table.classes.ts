import { SixtyStonePager } from '@sixty.stone/ngx-sixty-stone-pager';

export class SixtyStoneTable {
  id: string;
  rows: Array<any>;
  filteredRows: Array<any>;
  columns: Array<SixtyStoneTableColumn>;
  hiddenColumns: Array<SixtyStoneTableColumn>;
  allColumns: Array<SixtyStoneTableColumn>;
  pager: SixtyStonePager;
  displayedRows: Array<any>;
  filters: Array<SixtyStoneTableFilter>;
  sortColumn: SixtyStoneTableColumn;
  filterText: string;
  selectedFilterColumnId: string;
  savedFilters: Array<SixtyStoneTableSavedFilter> = [];
  selectedSavedFilterId: number = 0;

  constructor(id: string, data: Array<any>) {
    this.id = id;
    this.rows = data || [];
    this.columns = [];
    this.hiddenColumns = [];
    this.allColumns = [];
    this.filteredRows = [];
    this.displayedRows = [];
    this.filters = [];
    this.filterText = "";
    this.selectedFilterColumnId = "";
    this.sortColumn = new SixtyStoneTableColumn("", 0);
    this.pager = new SixtyStonePager(0, 0, 0);
  }
}

export class SixtyStoneTableColumn {
  columnId: string;
  sortOrder: number;

  constructor(id: string, order: number = 1) {
    this.columnId = id;
    this.sortOrder = order;
  }
}

export class SixtyStoneTableFilter {
  columnId: string;
  filterText: string;

  constructor(columnId: string, filterText: string) {
    this.columnId = columnId;
    this.filterText = filterText;
  }
}

export class SixtyStoneTableSavedFilter {
  id: number;
  name: string;
  filters: Array<SixtyStoneTableFilter>;

  constructor(id: number, name: string, filters: Array<SixtyStoneTableFilter>) {
    this.id = id;
    this.name = name;
    this.filters = filters;
  }
}

export class SixtyStoneTableStateData {
  id: string;
  currentPage: number;
  firstDisplayedPageNumber: number;
  filters: Array<SixtyStoneTableFilter>;
  sortColumn: SixtyStoneTableColumn;
  selectedFilterColumnId: string;
  selectedSavedFilterId: number;

  constructor(table: SixtyStoneTable) {
    this.id = table.id;
    this.currentPage = table.pager.currentPage;
    this.firstDisplayedPageNumber = table.pager.firstDisplayedPageNumber;
    this.filters = table.filters;
    this.sortColumn = table.sortColumn;
    this.selectedFilterColumnId = table.selectedFilterColumnId;
    this.selectedSavedFilterId = table.selectedSavedFilterId;
  }
}

export class SixtyStoneTableParams {
  dataSource: Array<any>;
  id: string;
  rowsPerPage: number;
  pageNumberDisplayCount: number;
  hiddenColumns: Array<SixtyStoneTableColumn>;
  eventHandlers: SixtyStoneTableEventHandlers;
  stateData: SixtyStoneTableStateData | null;
  savedFilters: Array<SixtyStoneTableSavedFilter> = [];

  static emptyParams() : SixtyStoneTableParams {
    return new SixtyStoneTableParams("", []);
  }

  constructor(id: string, dataSource: Array<any>, hiddenColumns: Array<SixtyStoneTableColumn> = [], rowsPerPage: number = 10, pageNumberDisplayCount: number = 10,
    stateData: SixtyStoneTableStateData | null = null) {
    this.id = id;
    this.dataSource = dataSource;    
    this.hiddenColumns = hiddenColumns;
    this.rowsPerPage = rowsPerPage;
    this.pageNumberDisplayCount = pageNumberDisplayCount;
    this.eventHandlers = new SixtyStoneTableEventHandlers();
    this.stateData = stateData;
  }
}
  
export class SixtyStoneTableEventHandlers {
  onCellClicked: Function = () : void => { }; // Params: column: SixtyStoneTableColumn, row: any

  constructor() { }
}
