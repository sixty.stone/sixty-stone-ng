import { TestBed } from '@angular/core/testing';

import { SixtyStoneTableHelper } from './sixty-stone-table-helper.service';

describe('SixtyStoneTableHelperService', () => {
  let service: SixtyStoneTableHelper;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SixtyStoneTableHelper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
