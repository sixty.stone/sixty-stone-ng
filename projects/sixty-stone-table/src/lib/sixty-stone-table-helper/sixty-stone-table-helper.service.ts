import { Injectable } from '@angular/core';
import { SixtyStoneTable, SixtyStoneTableColumn, SixtyStoneTableFilter, SixtyStoneTableStateData, SixtyStoneTableParams } from '../sixty-stone-table.classes';

@Injectable({
  providedIn: 'root'
})
export class SixtyStoneTableHelper {

  stateData: Array<SixtyStoneTableStateData> = [];

  buildTable(tableParams: SixtyStoneTableParams) : SixtyStoneTable {
    let table = new SixtyStoneTable(tableParams.id, tableParams.dataSource);

    if (table.rows.length === 0) {
      return table;
    }

    table.hiddenColumns = tableParams.hiddenColumns;

    for (const property in table.rows[0]) {
      const column = new SixtyStoneTableColumn(property, 1);

      table.allColumns.push(column);

      if (!tableParams.hiddenColumns.some(c => c.columnId == property)) {
        table.columns.push(column);
      }
    }

    table.sortColumn = table.allColumns[0];
    table.allColumns = table.allColumns.sort((c1, c2) => c1.columnId > c2.columnId ? 1 : -1);
    table.selectedFilterColumnId = table.allColumns[0].columnId;
    table.savedFilters = tableParams.savedFilters;

    if (tableParams.stateData) {
      table.filters = tableParams.stateData.filters;
      table.sortColumn = tableParams.stateData.sortColumn;
      table.selectedFilterColumnId = tableParams.stateData.selectedFilterColumnId;
      table.selectedSavedFilterId = tableParams.stateData.selectedSavedFilterId;
      if (table.filters.some(f => f.columnId === table.selectedFilterColumnId)) {
        table.filterText = table.filters.filter(f => f.columnId === table.selectedFilterColumnId)[0].filterText;
      }
    }

    if (table.selectedSavedFilterId === 0 && table.savedFilters.length > 0) {
      table.selectedSavedFilterId = table.savedFilters[0].id;
    }

    this.filter(table);
    this.sort(table);

    return table;
  }  

  updateTable(table: SixtyStoneTable, filterColumnId: string, filterText: string, sortColumn?: SixtyStoneTableColumn) : void {
    if (filterColumnId) {
      table.filteredRows = table.rows;
      
      const trimmedFilterText = filterText.trim().toLowerCase();

      if (table.filters.some(f => f.columnId === filterColumnId)) {
        const currentFilter = table.filters.filter(f => f.columnId == filterColumnId)[0];
        currentFilter.filterText = trimmedFilterText;
      }
      else {
        table.filters.push(new SixtyStoneTableFilter(filterColumnId, filterText));
      }

      if (trimmedFilterText === "") {
        table.filters = table.filters.filter(f => f.columnId !== filterColumnId);
      }

      if (table.filters.some(f => f.columnId === table.selectedFilterColumnId)) {
        table.filterText = table.filters.filter(f => f.columnId === table.selectedFilterColumnId)[0].filterText;
      }

      this.filter(table);
    }

    if (sortColumn) {
      sortColumn.sortOrder = -1 * sortColumn.sortOrder;
      table.sortColumn = sortColumn;
    }

    this.sort(table);
  }

  refreshDisplayedRows(table: SixtyStoneTable) : void {
    if (table.pager.totalPageCount === 1) {
      table.displayedRows = table.filteredRows;
    }
    else {
      table.displayedRows = [];
        for (let i = table.pager.displayRowsStartIndex; i <= table.pager.displayRowsEndIndex; i++) {
          table.displayedRows[table.displayedRows.length] = table.filteredRows[i];
        }
    }
  }

  private filter(table: SixtyStoneTable) : void {
    table.filteredRows = table.rows;
    if (table.filters.length > 0) {
      table.filters.forEach(f => table.filteredRows = table.filteredRows.filter(row => (row[f.columnId]?.toString() || "").toLowerCase().search(f.filterText) > -1));
    }
  }

  private sort(table: SixtyStoneTable) : void {
    table.filteredRows = table.filteredRows.sort((x, y) => (x[table.sortColumn.columnId]?.toString() || "").toLowerCase() > (y[table.sortColumn.columnId]?.toString() || "").toLowerCase() ? table.sortColumn.sortOrder : -1 * table.sortColumn.sortOrder);
  }

  constructor() { }
}
