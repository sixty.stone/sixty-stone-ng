import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SixtyStonePagerModule } from '@sixty.stone/ngx-sixty-stone-pager';
import { SixtyStoneDialogModule } from '@sixty.stone/ngx-sixty-stone-dialog';
import { SixtyStoneTableComponent } from './sixty-stone-table-component/sixty-stone-table.component';

@NgModule({
  declarations: [
    SixtyStoneTableComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    SixtyStonePagerModule,
    SixtyStoneDialogModule
  ],
  exports: [
    SixtyStoneTableComponent
  ]
})
export class SixtyStoneTableModule { }
