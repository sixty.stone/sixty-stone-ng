# @sixty.stone/ngx-sixty-stone-dialog

## Implementation

### Bootstrap Module

* `import { SixtyStoneDialogModule } from '@sixty.stone/ngx-sixty-stone-dialog'`

### App Component TypeScript

* `import { SixtyStoneDialogComponent, SixtyStoneDialogParams, SixtyStoneDialogSize } from '@sixty.stone/ngx-sixty-stone-dialog';`

* `private dialog : SixtyStoneDialogComponent | null = null;`

* `dialogParams: SixtyStoneDialogParams = new SixtyStoneDialogParams(SixtyStoneDialogSize.medium, "Title");`

### Inside ngOnInit()

* `this.dialogParams.eventHandlers.onInit = (d: SixtyStoneDialogComponent) => this.dialog = d;`

### Inside Show Handler

* `this.dialog?.open();`

### App Component HTML

* `<sixty-stone-dialog [dialog-params]="dialogParams">[put some HTML in here]</sixty-stone-dialog>`
