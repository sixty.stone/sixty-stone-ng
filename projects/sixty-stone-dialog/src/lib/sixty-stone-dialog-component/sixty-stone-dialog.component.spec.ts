import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SixtyStoneDialogComponent } from './sixty-stone-dialog.component';

describe('SixtyStoneDialogComponent', () => {
  let component: SixtyStoneDialogComponent;
  let fixture: ComponentFixture<SixtyStoneDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SixtyStoneDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SixtyStoneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
