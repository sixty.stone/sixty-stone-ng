import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sixty-stone-dialog',
  templateUrl: './sixty-stone-dialog.component.html',
  styleUrls: ['./sixty-stone-dialog.component.sass']
})
export class SixtyStoneDialogComponent implements OnInit {

  @Input("dialog-params") dialogParams : SixtyStoneDialogParams = SixtyStoneDialogParams.emptyParams();

  isVisible: boolean = false;
  size: SixtyStoneDialogSize = SixtyStoneDialogSize.small;
  title: string = "";

  open() : void {
    this.isVisible = true;
  }

  close() : void {
    this.isVisible = false;
    this.dialogParams.eventHandlers.onClose();
  }

  ngOnInit(): void {
    this.size = this.dialogParams.size;
    this.title = this.dialogParams.title;
    this.dialogParams.eventHandlers.onInit(this);
  }

  constructor() { }

}

export enum SixtyStoneDialogSize {
  small = "dialog-small",
  medium = "dialog-medium",
  large = "dialog-large",
  fullScreen = "dialog-full"
}

export class SixtyStoneDialogParams {
  size: SixtyStoneDialogSize;
  title: string;
  eventHandlers: SixtyStoneDialogEventHandlers;

  static emptyParams() : SixtyStoneDialogParams {
    return new SixtyStoneDialogParams(SixtyStoneDialogSize.small,"");
  }

  constructor(size: SixtyStoneDialogSize, title: string) {
    this.size = size;
    this.title = title;
    this.eventHandlers = new SixtyStoneDialogEventHandlers();
  }
}

class SixtyStoneDialogEventHandlers {
  onInit: Function = () : void => { }; // Params: dialogBox: SixtyStoneDialogComponent
  onClose: Function = () : void => { };

  constructor() { }
}
