import { NgModule } from '@angular/core';
import { SixtyStoneDialogComponent } from './sixty-stone-dialog-component/sixty-stone-dialog.component';

@NgModule({
  declarations: [
    SixtyStoneDialogComponent
  ],
  imports: [
  ],
  exports: [
    SixtyStoneDialogComponent
  ]
})
export class SixtyStoneDialogModule { }
