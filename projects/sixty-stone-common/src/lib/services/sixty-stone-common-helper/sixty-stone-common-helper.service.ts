import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SixtyStoneCommonHelper {

  clone<T>(source: T) : T {
    return JSON.parse(JSON.stringify(source));
  }

  constructor() { }
}
