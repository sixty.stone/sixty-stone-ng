import { TestBed } from '@angular/core/testing';
import { SixtyStoneCommonHelper } from './sixty-stone-common-helper.service';

describe('SixtyStoneCommonHelperService', () => {
  let service: SixtyStoneCommonHelper;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SixtyStoneCommonHelper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe("clone", () => {

    it("should clone input", () => {
      const input = {
        Property1: [1,2,3,4,5],
        Property2: {
          NestedProperty1: 42,
          NestedProperty2: 73
        },
        Property3: 62,
        Property4: "one ring to rule them all"
      };

      const result = service.clone(input);

      expect(result.Property1).toEqual(input.Property1);
      expect(result.Property2.NestedProperty1).toBe(input.Property2.NestedProperty1);
      expect(result.Property2.NestedProperty2).toBe(input.Property2.NestedProperty2);
      expect(result.Property3).toBe(input.Property3);
      expect(result.Property4).toBe(input.Property4);
    });

    it("should handle null", () => {
      const result = service.clone(null);
      expect(result).toBeNull();
    });

  });

});
