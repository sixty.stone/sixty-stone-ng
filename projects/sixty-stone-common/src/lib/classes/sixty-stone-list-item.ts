export class SixtyStoneListItem<T> {
    value: number;
    text: string;
    isSelected: boolean;
    parent: T;
  
    static emptyListItem<T>(parent: T) : SixtyStoneListItem<T> {
      return new SixtyStoneListItem(0, "", false, parent);
    }
  
    constructor(value: number, text: string, isSelected: boolean, parent: T) {
      this.value = value;
      this.text = text;
      this.isSelected = isSelected;
      this.parent = parent;
    }
  }
  