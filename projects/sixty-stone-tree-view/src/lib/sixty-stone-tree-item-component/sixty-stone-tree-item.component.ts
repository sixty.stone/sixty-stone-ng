import { Component, OnInit, Input } from '@angular/core';
import { SixtyStoneTreeItem } from '../sixty-stone-tree-view.classes';
import { SixtyStoneTreeViewHelper } from '../sixty-stone-tree-view-helper/sixty-stone-tree-view-helper.service';

@Component({
  selector: 'sixty-stone-tree-item',
  templateUrl: './sixty-stone-tree-item.component.html',
  styleUrls: ['./sixty-stone-tree-item.component.sass']
})
export class SixtyStoneTreeItemComponent implements OnInit {

  @Input("tree-item") inputTreeItem: SixtyStoneTreeItem = SixtyStoneTreeItem.emptyItem();

  treeItem: SixtyStoneTreeItem = SixtyStoneTreeItem.emptyItem();
  folderIconPath: string = "";
  openFolderIconPath: string = "";
  plusIconPath: string = "";
  minusIconPath: string = "";
  nodeIconPath: string = "";
  lineIconPath: string = "";

  toggleNode(node: SixtyStoneTreeItem) : void {
    this.treeHelper.toggleNode(node);
  }

  ngOnInit(): void {
    this.treeItem = this.inputTreeItem;
    const paths = this.treeHelper.getIconPaths(this.treeItem.TreeId);
    this.folderIconPath = paths.FolderIconPath;
    this.openFolderIconPath = paths.OpenFolderIconPath;
    this.plusIconPath = paths.PlusIconPath;
    this.minusIconPath = paths.MinusIconPath;
    this.nodeIconPath = paths.NodeIconPath;
    this.lineIconPath = paths.LineIconPath;
  }

  constructor(private treeHelper: SixtyStoneTreeViewHelper) { }
}
