import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SixtyStoneTreeItemComponent } from './sixty-stone-tree-item.component';

describe('SixtyStoneTreeItemComponent', () => {
  let component: SixtyStoneTreeItemComponent;
  let fixture: ComponentFixture<SixtyStoneTreeItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SixtyStoneTreeItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SixtyStoneTreeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
