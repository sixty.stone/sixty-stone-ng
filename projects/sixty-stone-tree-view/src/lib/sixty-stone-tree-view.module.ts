import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SixtyStoneTreeItemComponent } from './sixty-stone-tree-item-component/sixty-stone-tree-item.component';
import { SixtyStoneTreeViewComponent } from './sixty-stone-tree-view-component/sixty-stone-tree-view.component';

@NgModule({
  declarations: [
    SixtyStoneTreeItemComponent,
    SixtyStoneTreeViewComponent
  ],
  imports: [
    BrowserModule,
    CommonModule
  ],
  exports: [
    SixtyStoneTreeViewComponent
  ]
})
export class SixtyStoneTreeViewModule { }
