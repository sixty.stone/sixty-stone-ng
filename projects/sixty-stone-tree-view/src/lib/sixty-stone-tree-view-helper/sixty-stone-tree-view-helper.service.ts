import { Injectable } from '@angular/core';
import { ISixtyStoneTreeItemBase, SixtyStoneTreeItem, SixtyStoneTreeView, SixtyStoneTreeViewEventHandlers,
  SixtyStoneTreeViewIconPaths, SixtyStoneTreeViewParams } from '../sixty-stone-tree-view.classes';

@Injectable({
  providedIn: 'root'
})
export class SixtyStoneTreeViewHelper {

  private nodeToggleDictionary: Record<string, SixtyStoneTreeViewEventHandlers> | null = null;
  private iconPathDictionary: Record<string, SixtyStoneTreeViewIconPaths> | null = null;

  buildTree(treeParams: SixtyStoneTreeViewParams) : SixtyStoneTreeView {
    const nodes: Array<SixtyStoneTreeItem> = this.HierarchichalNodes(treeParams.Items, 0);
    const result = new SixtyStoneTreeView(treeParams.Id, nodes);
    result.PlusIconPath = treeParams.IconPaths.PlusIconPath;
    result.MinusIconPath = treeParams.IconPaths.MinusIconPath;
    return result;
  }

  subscribe(treeId: string, handlers: SixtyStoneTreeViewEventHandlers) : void {
    if (this.nodeToggleDictionary === null) {
      this.nodeToggleDictionary = {treeId: handlers};
    }
    else {
      this.nodeToggleDictionary[treeId] = handlers;
    }
  }

  toggleNode(node: SixtyStoneTreeItem) : void {
    node.Collapsed = !node.Collapsed;
    if (this.nodeToggleDictionary !== null) {
      const handlers = this.nodeToggleDictionary[node.TreeId];
      if (handlers) {
        handlers.OnNodeToggle(node);
      }
    }
  }

  expandTree(tree: SixtyStoneTreeView) : void {
    this.expandCollapse(tree, false);
  }

  collapseTree(tree: SixtyStoneTreeView) : void {
    this.expandCollapse(tree, true);
  }

  registerIconPaths(treeId: string, paths: SixtyStoneTreeViewIconPaths) : void {
    if (this.iconPathDictionary === null) {
      this.iconPathDictionary = {treeId: paths};
    }
    else {
      this.iconPathDictionary[treeId] = paths;
    }
  }

  getIconPaths(treeId: string) : SixtyStoneTreeViewIconPaths {
    if (this.iconPathDictionary) {
      return this.iconPathDictionary[treeId] || new SixtyStoneTreeViewIconPaths();
    }
    return new SixtyStoneTreeViewIconPaths();
  }

  private HierarchichalNodes(items: Array<ISixtyStoneTreeItemBase>, parentKey: number) : Array<SixtyStoneTreeItem> {
    const context = this;
    const nodes = items.filter(node => node.ParentKey === parentKey)
      .map(node => new SixtyStoneTreeItem(node, true));
    nodes.forEach(node => {
      node.Children = context.HierarchichalNodes(items, node.TreeKey);
      node.HasChildren = node.Children.length > 0;
    });
    return nodes;
  }

  private expandCollapse(tree: SixtyStoneTreeView, collapsed: boolean) : void {
    tree.Collapsed = collapsed;
    tree.Items.forEach(node => this.expandCollapseAll(node, collapsed));
  }

  private expandCollapseAll(node: SixtyStoneTreeItem, collapsed: boolean) : void {
    if (node.HasChildren) {
      node.Collapsed = collapsed;
      node.Children.forEach(child => this.expandCollapseAll(child, collapsed));
    }
  }

  constructor() { }
}
