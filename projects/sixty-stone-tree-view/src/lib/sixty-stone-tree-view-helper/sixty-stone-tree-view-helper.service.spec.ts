import { TestBed } from '@angular/core/testing';

import { SixtyStoneTreeViewHelper } from './sixty-stone-tree-view-helper.service';

describe('SixtyStoneTreeViewHelperService', () => {
  let service: SixtyStoneTreeViewHelper;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SixtyStoneTreeViewHelper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
