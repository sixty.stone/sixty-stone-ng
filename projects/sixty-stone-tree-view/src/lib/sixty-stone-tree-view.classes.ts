export interface ISixtyStoneTreeItemBase {
  TreeId: string;
  TreeKey: number;
  ParentKey: number;
  Title: string;
}

export class SixtyStoneTreeItem implements ISixtyStoneTreeItemBase {
  TreeId: string;
  TreeKey: number;
  ParentKey: number;
  Title: string;
  Collapsed: boolean;
  HasChildren: boolean = false;
  Children: Array<SixtyStoneTreeItem> = [];

  static emptyItem(): SixtyStoneTreeItem {
    return new SixtyStoneTreeItem({TreeId: "", TreeKey: 0, ParentKey: 0, Title: ""}, true);
  }

  constructor(itemBase: ISixtyStoneTreeItemBase, collapsed: boolean) {
    this.TreeId = itemBase.TreeId;
    this.TreeKey = itemBase.TreeKey;
    this.ParentKey = itemBase.ParentKey;
    this.Title = itemBase.Title;
    this.Collapsed = collapsed;
  }
}

export class SixtyStoneTreeView {
  Id: string;
  Items: Array<SixtyStoneTreeItem>;
  Collapsed: boolean = true;
  PlusIconPath: string = "";
  MinusIconPath: string = "";

  static emptyTree(): SixtyStoneTreeView {
    return new SixtyStoneTreeView("", []);
  }

  constructor(id: string, items: Array<SixtyStoneTreeItem>) {
    this.Id = id;
    this.Items = items;
  }
}

export class SixtyStoneTreeViewParams {
  Id: string;
  Items: Array<ISixtyStoneTreeItemBase>;
  EventHandlers: SixtyStoneTreeViewEventHandlers = new SixtyStoneTreeViewEventHandlers();
  IconPaths: SixtyStoneTreeViewIconPaths = new SixtyStoneTreeViewIconPaths();

  static emptyParams(): SixtyStoneTreeViewParams {
    return new SixtyStoneTreeViewParams("", []);
  }

  constructor(id: string, items: Array<ISixtyStoneTreeItemBase>) {
    this.Id = id;
    this.Items = items;
  }
}

export class SixtyStoneTreeViewEventHandlers {
  OnNodeToggle: Function = (): void => {}; // Parameters: node: SixtyStoneTreeItem
  OnNodeClick: Function = (): void => {}; // Parameters: node: SixtyStoneTreeItem
}

export class SixtyStoneTreeViewIconPaths {
  FolderIconPath: string = "/assets/folder.jpg";
  OpenFolderIconPath: string = "/assets/open-folder.jpg";
  PlusIconPath: string = "/assets/plus.jpg";
  MinusIconPath: string = "/assets/minus.jpg";
  NodeIconPath: string = "/assets/node.jpg";
  LineIconPath: string = "/assets/line.jpg";
}
