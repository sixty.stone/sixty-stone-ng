import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { SixtyStoneTreeView, SixtyStoneTreeViewParams } from '../sixty-stone-tree-view.classes';
import { SixtyStoneTreeViewHelper } from '../sixty-stone-tree-view-helper/sixty-stone-tree-view-helper.service';

@Component({
  selector: 'sixty-stone-tree-view',
  templateUrl: './sixty-stone-tree-view.component.html',
  styleUrls: ['./sixty-stone-tree-view.component.sass']
})
export class SixtyStoneTreeViewComponent implements OnInit {

  @Input("tree-view-params") treeViewParams: SixtyStoneTreeViewParams = SixtyStoneTreeViewParams.emptyParams();

  tree: SixtyStoneTreeView = SixtyStoneTreeView.emptyTree();

  toggleAll(): void {
    if (this.tree.Collapsed) {
      this.treeHelper.expandTree(this.tree);
    }
    else {
      this.treeHelper.collapseTree(this.tree);
    }
  }

  ngOnInit(): void {
    this.make();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.treeViewParams && typeof changes.treeViewParams.currentValue === "object") {
      this.make();
    }
  }

  private make(): void {
    this.tree = this.treeHelper.buildTree(this.treeViewParams);
    this.treeHelper.subscribe(this.treeViewParams.Id, this.treeViewParams.EventHandlers);
    this.treeHelper.registerIconPaths(this.treeViewParams.Id, this.treeViewParams.IconPaths);
  }

  constructor(private treeHelper: SixtyStoneTreeViewHelper) { }
}
