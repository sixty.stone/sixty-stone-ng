import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SixtyStoneTreeViewComponent } from './sixty-stone-tree-view.component';

describe('SixtyStoneTreeViewComponent', () => {
  let component: SixtyStoneTreeViewComponent;
  let fixture: ComponentFixture<SixtyStoneTreeViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SixtyStoneTreeViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SixtyStoneTreeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
