import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SixtyStoneMultiSelectComponent } from './sixty-stone-multi-select-component/sixty-stone-multi-select.component'

@NgModule({
  declarations: [
    SixtyStoneMultiSelectComponent
  ],
  imports: [
    BrowserModule,
    CommonModule
  ],
  exports: [
    SixtyStoneMultiSelectComponent
  ]
})
export class SixtyStoneMultiSelectModule { }
