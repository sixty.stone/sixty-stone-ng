import { TestBed } from '@angular/core/testing';

import { SixtyStoneMultiSelectHelper } from './sixty-stone-multi-select-helper.service';

describe('SixtyStoneMultiSelectHelperService', () => {
  let service: SixtyStoneMultiSelectHelper;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SixtyStoneMultiSelectHelper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
