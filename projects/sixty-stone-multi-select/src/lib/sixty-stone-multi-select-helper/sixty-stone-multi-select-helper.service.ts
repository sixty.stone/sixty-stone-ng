import { Injectable } from '@angular/core';
import { SixtyStoneListItem } from '@sixty.stone/ngx-sixty-stone-common';
import { SixtyStoneMultiSelectParams } from '../sixty-stone-multi-select-component/sixty-stone-multi-select.component';

@Injectable({
  providedIn: 'root'
})
export class SixtyStoneMultiSelectHelper {

  add<T>(multiSelectParams: SixtyStoneMultiSelectParams<T>) : void {
    let addedItems: Array<SixtyStoneListItem<T>> = [];
    if (multiSelectParams.isCustomList) {
      if (!multiSelectParams.itemsAdded.some(item => item.text === multiSelectParams.customListText)) {
        addedItems.push(new SixtyStoneListItem<T>(0, multiSelectParams.customListText, false, null as any));
        multiSelectParams.itemsAdded.push(addedItems[0]);
      }
    }
    else {
      multiSelectParams.itemsNotAdded.filter(i => i.isSelected).forEach(i => {
        addedItems.push(i);
        multiSelectParams.itemsAdded.push(i);
      });
      addedItems.forEach(i => {
        let index = multiSelectParams.itemsNotAdded.indexOf(i);
        multiSelectParams.itemsNotAdded.splice(index, 1);
        i.isSelected = false;
      });
    }
    this.sortItems(multiSelectParams.itemsAdded);
    multiSelectParams.eventHandlers.onAdd(addedItems);
    multiSelectParams.eventHandlers.onChange(addedItems, []);
  }

  remove<T>(multiSelectParams: SixtyStoneMultiSelectParams<T>) : void {
    let removedItems: Array<SixtyStoneListItem<T>> = [];
    multiSelectParams.itemsAdded.filter(i => i.isSelected).forEach(i => {
      removedItems.push(i);
      if (!multiSelectParams.isCustomList) {
        multiSelectParams.itemsNotAdded.push(i);
      }
    });
    removedItems.forEach(i => {
      let index = multiSelectParams.itemsAdded.indexOf(i);
      multiSelectParams.itemsAdded.splice(index, 1);
      i.isSelected = false;
    });
    this.sortItems(multiSelectParams.itemsNotAdded);
    multiSelectParams.eventHandlers.onRemove(removedItems);
    multiSelectParams.eventHandlers.onChange([], removedItems);
  }

  itemSelected<T>(item: SixtyStoneListItem<T>) : void {
    item.isSelected = !item.isSelected;
  }

  private sortItems<T>(items: Array<SixtyStoneListItem<T>>) : void {
    items = items.sort((item1: SixtyStoneListItem<T>, item2: SixtyStoneListItem<T>) => {
      return item1.text > item2.text ? 1 : -1;
    });
  }

  constructor() { }
}
