import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { SixtyStoneListItem } from '@sixty.stone/ngx-sixty-stone-common';
import { SixtyStoneMultiSelectHelper } from '../sixty-stone-multi-select-helper/sixty-stone-multi-select-helper.service';

@Component({
  selector: 'sixty-stone-multi-select',
  templateUrl: './sixty-stone-multi-select.component.html',
  styleUrls: ['./sixty-stone-multi-select.component.sass']
})
export class SixtyStoneMultiSelectComponent<T> implements OnInit {

  @Input("multi-select-params") multiSelectParams: SixtyStoneMultiSelectParams<T> = SixtyStoneMultiSelectParams.emptyParams<T>();

  id: string = "";
  itemsAdded: Array<SixtyStoneListItem<T>> = [];
  itemsNotAdded: Array<SixtyStoneListItem<T>> = [];
  isReadonly: boolean = false;
  isCustomList: boolean = false;
  customListText: string = "";

  itemSelected(item: SixtyStoneListItem<T>) : void {
    if (this.isReadonly) {
      return;
    }
    this.multiSelectHelper.itemSelected(item);
  }

  add() : void {
    if (this.isReadonly) {
      return;
    }
    this.multiSelectHelper.add(this.multiSelectParams);
    this.customListText = "";
  }

  remove() : void {
    if (this.isReadonly) {
      return;
    }
    this.multiSelectHelper.remove(this.multiSelectParams);
  }

  onCustomListTextChanged(customListText: string) : void {
    this.customListText = customListText;
    this.multiSelectParams.customListText = customListText;
  }

  ngOnInit(): void {
    this.setProperties(this.multiSelectParams);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.multiSelectParams && typeof changes.multiSelectParams.currentValue === "object") {
      this.setProperties(changes.multiSelectParams.currentValue);
    }
  }

  private setProperties(params: SixtyStoneMultiSelectParams<T>) : void {
    if (params) {
      this.id = params.id;
      this.itemsAdded = params.itemsAdded;
      this.itemsNotAdded = params.itemsNotAdded;
      this.isReadonly = params.isReadonly;
      this.isCustomList = params.isCustomList;
    }
  }

  constructor(private multiSelectHelper: SixtyStoneMultiSelectHelper) { }
}

export class SixtyStoneMultiSelectParams<T> {
  id: string;
  itemsAdded: Array<SixtyStoneListItem<T>>;
  itemsNotAdded: Array<SixtyStoneListItem<T>>;
  eventHandlers: SixtyStoneMultiSelectEventHandlers<T>;
  isReadonly: boolean = false;
  isCustomList: boolean = false;
  customListText: string = "";

  static emptyParams<T>() : SixtyStoneMultiSelectParams<T> {
    return new SixtyStoneMultiSelectParams<T>("", [], []);
  }

  constructor (id: string, itemsNotAdded: Array<SixtyStoneListItem<T>>, itemsAdded: Array<SixtyStoneListItem<T>>) {
    this.id = id;
    this.itemsAdded = itemsAdded;
    this.itemsNotAdded = itemsNotAdded;
    this.eventHandlers = new SixtyStoneMultiSelectEventHandlers();
  }
}

export class SixtyStoneMultiSelectEventHandlers<T> {
  onAdd: Function = () : void => { }; // Params: addedItems: Array<SixtyStoneListItem<T>>
  onRemove: Function = () : void => { }; // Params: removedItems: Array<SixtyStoneListItem<T>>
  onChange: Function = () : void => { }; // Params: addedItems: Array<SixtyStoneListItem<T>>, removedItems: Array<SixtyStoneListItem<T>>

  constructor() { }
}
