import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SixtyStoneMultiSelectComponent } from './sixty-stone-multi-select.component';

describe('SixtyStoneMultiSelectComponent', () => {
  let component: SixtyStoneMultiSelectComponent;
  let fixture: ComponentFixture<SixtyStoneMultiSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SixtyStoneMultiSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SixtyStoneMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
