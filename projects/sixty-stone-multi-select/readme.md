# @sixty.stone/ngx-sixty-stone-multi-select

## Dependencies

* `npm install @sixty.stone/ngx-sixty-stone-common --save`

## Implementation

### Bootstrap Module

* `import { SixtyStoneMultiSelectModule } from '@sixty.stone/ngx-sixty-stone-multi-select'`

### App Component TypeScript

* `import { SixtyStoneListItem } from '@sixty.stone/ngx-sixty-stone-common';`

* `import { SixtyStoneMultiSelectParams } from 'sixty-stone-multi-select';`

* `multiSelectParams: SixtyStoneMultiSelectParams<DemoListItem> = SixtyStoneMultiSelectParams.emptyParams();`

### Inside ngOnInit()

* `this.multiSelectParams = new SixtyStoneMultiSelectParams(id, data);`

* `this.multiSelectParams.eventHandlers.onChange = (addedItems, removedItems) => console.log({addedItems, removedItems});`

### App Component HTML

* `<sixty-stone-multi-select [multi-select-params]="multiSelectParams"></sixty-stone-multi-select>`
