# @sixty.stone/ngx-sixty-stone-pager

## Implementation

### Bootstrap Module

* `import { SixtyStonePagerModule } from '@sixty.stone/ngx-sixty-stone-pager'`

### App Component TypeScript

* `import { SixtyStonePager, SixtyStonePagerParams } from '@sixty.stone/ngx-sixty-stone-pager';`

* `pagerParams: SixtyStonePagerParams = SixtyStonePagerParams.emptyParams();`

### Inside ngOnInit()

* `const pagerParams = new SixtyStonePagerParams(rowCount, rowsPerPage, pageNumberDisplayCount, currentPage, firstDisplayedPageNumber);`

* `pagerParams.eventHandlers.onPageEvent = (SixtyStonePager pager) => console.log(pager);`

* `this.pagerParams = pagerParams;`

### App Component HTML

* `<sixty-stone-pager [pager-params]="pagerParams"></sixty-stone-pager>`
