/*
 * Public API Surface of sixty-stone-pager
 */

export * from './lib/sixty-stone-pager.classes';
export * from './lib/sixty-stone-pager-component/sixty-stone-pager.component';
export * from './lib/sixty-stone-pager.module';
