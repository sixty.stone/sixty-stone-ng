import { TestBed } from '@angular/core/testing';

import { SixtyStonePagerHelper } from './sixty-stone-pager-helper.service';

describe('SixtyStonePagerHelperService', () => {
  let service: SixtyStonePagerHelper;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SixtyStonePagerHelper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
