import { Injectable } from '@angular/core';
import { SixtyStonePager, SixtyStonePagerParams } from '../sixty-stone-pager.classes';

@Injectable({
  providedIn: 'root'
})
export class SixtyStonePagerHelper {

  buildPager(pagerParams: SixtyStonePagerParams): SixtyStonePager {
    let pager = new SixtyStonePager(pagerParams.rowsPerPage, pagerParams.pageNumberDisplayCount, pagerParams.currentPage);

    pager.totalRowCount = pagerParams.totalRowCount;
    pager.firstDisplayedPageNumber = pagerParams.firstDisplayedPageNumber;

    if (pager.firstDisplayedPageNumber <= pager.currentPage - pager.pageNumberDisplayCount) {
      pager.firstDisplayedPageNumber = pager.currentPage - pager.pageNumberDisplayCount + 1;
    }

    if (pager.totalRowCount > pager.rowsPerPage) {
      pager.totalPageCount = Math.floor(pager.totalRowCount / pager.rowsPerPage);
      if (pager.totalRowCount % pager.rowsPerPage !== 0) {
        pager.totalPageCount += 1;
      }
    }
    else {
      pager.totalPageCount = 1;
    }

    if (pager.currentPage > pager.totalPageCount) {
      pager.currentPage = pager.totalPageCount;
      pager.firstDisplayedPageNumber = pager.totalPageCount - pager.pageNumberDisplayCount + 1;
    }

    if (pager.firstDisplayedPageNumber < 1) {
      pager.firstDisplayedPageNumber = 1;
    }

    this.refreshPagerDisplay(pager);

    return pager;
  }

  goToPage(pager: SixtyStonePager, pageNumber: number) : void {
    if (pageNumber < 1 || pageNumber > pager.totalPageCount) {
      return;
    }

    pager.currentPage = pageNumber;

    const lastDisplayedPageNumber = pager.displayedPageNumbers[pager.displayedPageNumbers.length -1];
    let firstDisplayedPageNumber = pager.displayedPageNumbers[0];    

    if (pager.currentPage < firstDisplayedPageNumber) firstDisplayedPageNumber = pager.currentPage;
    if (pager.currentPage > lastDisplayedPageNumber) firstDisplayedPageNumber = pager.currentPage - pager.pageNumberDisplayCount + 1;

    pager.firstDisplayedPageNumber = firstDisplayedPageNumber;

    this.refreshPagerDisplay(pager);
  }

  skip(pager: SixtyStonePager, bAhead: boolean) : void {
    if (pager.totalPageCount <= pager.pageNumberDisplayCount) {
      return;
    }

    if (pager.currentPage === 1 && !bAhead) {
      return;
    }

    if (pager.currentPage === pager.totalPageCount && bAhead) {
      return;
    }

    const firstDisplayedPageNumber = pager.displayedPageNumbers[0];
    const lastDisplayedPageNumber = pager.displayedPageNumbers[pager.displayedPageNumbers.length -1];

    if (firstDisplayedPageNumber === 1 && !bAhead) {
      return;
    }

    if (lastDisplayedPageNumber === pager.totalPageCount && bAhead) {
      return;
    }

    const lastPossibleFirstDisplayedPageNumber = pager.totalPageCount - pager.pageNumberDisplayCount + 1;

    if (bAhead) {
      const target = firstDisplayedPageNumber + pager.pageNumberDisplayCount;

      pager.firstDisplayedPageNumber = target <= lastPossibleFirstDisplayedPageNumber ? target : lastPossibleFirstDisplayedPageNumber;
      pager.currentPage = pager.firstDisplayedPageNumber;
    }
    else {
      const target = firstDisplayedPageNumber - pager.pageNumberDisplayCount;

      pager.firstDisplayedPageNumber = target > 1 ? target : 1;
      pager.currentPage = pager.firstDisplayedPageNumber + pager.pageNumberDisplayCount - 1;
    }

    this.refreshPagerDisplay(pager);
  }

  private refreshPagerDisplay(pager: SixtyStonePager): void {
    pager.displayedPageNumbers = [];

    for (let i = pager.firstDisplayedPageNumber; i < pager.firstDisplayedPageNumber + pager.pageNumberDisplayCount; i++) {
      pager.displayedPageNumbers[pager.displayedPageNumbers.length] = i;

      if (i === pager.totalPageCount) {
        break;
      }
    }

    pager.displayRowsStartIndex = pager.rowsPerPage * (pager.currentPage - 1);
    pager.displayRowsEndIndex = pager.rowsPerPage * pager.currentPage - 1;

    if (pager.displayRowsEndIndex > pager.totalRowCount - 1) {
      pager.displayRowsEndIndex = pager.totalRowCount - 1;
    }
  }

  constructor() { }
}
