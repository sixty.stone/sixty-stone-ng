export class SixtyStonePager {
    pageNumberDisplayCount: number;
    rowsPerPage: number;
    currentPage: number;
    firstDisplayedPageNumber: number;
    totalRowCount: number;
    totalPageCount: number;
    displayedPageNumbers: Array<number>;
    displayRowsStartIndex: number;
    displayRowsEndIndex: number;
  
    static emptyPager() : SixtyStonePager {
      return new SixtyStonePager();
    }
  
      constructor(rowsPerPage: number = 10, pageNumberDisplayCount: number = 10, currentPage: number = 1) {
          this.rowsPerPage = rowsPerPage;
          this.pageNumberDisplayCount = pageNumberDisplayCount;
          this.currentPage = currentPage;
      this.firstDisplayedPageNumber = 0;
      this.totalRowCount = 0;
      this.totalPageCount = 0;
      this.displayedPageNumbers = [];
      this.displayRowsStartIndex = 0;
      this.displayRowsEndIndex = 0;
      }
  }
  
  export class SixtyStonePagerParams {
    pageNumberDisplayCount: number;
    rowsPerPage: number;
    currentPage: number;
    firstDisplayedPageNumber: number;
    totalRowCount: number;
    eventHandlers: SixtyStonePagerEventHandlers;
  
    static emptyParams(): SixtyStonePagerParams {
      return new SixtyStonePagerParams(0);
    }
  
      constructor(totalRowCount: number, rowsPerPage: number = 10, pageNumberDisplayCount: number = 10, currentPage: number = 1, firstDisplayedPageNumber: number = 1) {
          this.rowsPerPage = rowsPerPage;
          this.pageNumberDisplayCount = pageNumberDisplayCount;
          this.currentPage = currentPage;
      this.firstDisplayedPageNumber = firstDisplayedPageNumber;
      this.totalRowCount = totalRowCount;
      this.eventHandlers = new SixtyStonePagerEventHandlers();
      }
  }
  
  export class SixtyStonePagerEventHandlers {
    onPageEvent: Function = () : void => { } // Params: pager: SixtyStonePager
  
    constructor () { }
  }
