import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { SixtyStonePager, SixtyStonePagerParams } from '../sixty-stone-pager.classes';
import { SixtyStonePagerHelper } from '../sixty-stone-pager-helper/sixty-stone-pager-helper.service';

@Component({
  selector: 'sixty-stone-pager',
  templateUrl: './sixty-stone-pager.component.html',
  styleUrls: ['./sixty-stone-pager.component.sass']
})
export class SixtyStonePagerComponent implements OnInit {

  @Input("pager-params") pagerParams: SixtyStonePagerParams = SixtyStonePagerParams.emptyParams();
  pager: SixtyStonePager = SixtyStonePager.emptyPager();

  goToPage(pageNumber: number) : void {
    this.pagerHelper.goToPage(this.pager, pageNumber);
    this.pagerParams.eventHandlers.onPageEvent(this.pager);
  }

  skip(isRight: boolean) : void {
    this.pagerHelper.skip(this.pager, isRight);
    this.pagerParams.eventHandlers.onPageEvent(this.pager);
  }

  ngOnInit(): void {
    this.pager = this.pagerHelper.buildPager(this.pagerParams);
    this.pagerParams.eventHandlers.onPageEvent(this.pager);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.pagerParams && typeof changes.pagerParams.currentValue === "object") {
      this.pager = this.pagerHelper.buildPager(this.pagerParams);
      this.pagerParams.eventHandlers.onPageEvent(this.pager);
    }
  }

  constructor(private pagerHelper: SixtyStonePagerHelper) { }
}
