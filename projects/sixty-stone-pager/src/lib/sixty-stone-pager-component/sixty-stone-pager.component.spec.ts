import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SixtyStonePagerComponent } from './sixty-stone-pager.component';

describe('SixtyStonePagerComponent', () => {
  let component: SixtyStonePagerComponent;
  let fixture: ComponentFixture<SixtyStonePagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SixtyStonePagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SixtyStonePagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
