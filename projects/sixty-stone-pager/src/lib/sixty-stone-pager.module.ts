import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SixtyStonePagerComponent } from './sixty-stone-pager-component/sixty-stone-pager.component';

@NgModule({
  declarations: [
    SixtyStonePagerComponent
  ],
  imports: [
    BrowserModule,
    CommonModule
  ],
  exports: [
    SixtyStonePagerComponent
  ]
})
export class SixtyStonePagerModule { }
