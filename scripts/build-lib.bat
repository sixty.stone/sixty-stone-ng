@echo off

call npm install

if exist .\dist\ (
  rmdir /q /s .\dist\
)

call ng build ngx-sixty-stone-common --configuration production

call ng build ngx-sixty-stone-dialog --configuration production

call ng build ngx-sixty-stone-pager --configuration production

call ng build ngx-sixty-stone-table --configuration production

call ng build ngx-sixty-stone-multi-select --configuration production

call ng build ngx-sixty-stone-tree-view --configuration production
